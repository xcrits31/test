<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
//use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
/**
 * Class ArticleController.
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article_index")
     */
    public function indexAction()
    {
       $em = $this->getDoctrine()->getManager();

  /*       $dql = "SELECT p, c FROM BlogPost p JOIN p.comments c";
        $query = $em->createQuery($dql)
            ->setFirstResult(0)
            ->setMaxResults(100);

        $paginator = new Paginator($query, $fetchJoinCollection = true);

        $c = count($paginator);
        foreach ($paginator as $post) {
            echo $post->getHeadline() . "\n";
        }*/
        $articles = $em->getRepository(Article::class)->findAll();
        return $this->render('article/index.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/article/create", name="article_create")
     */
    public function createAction(Request $request, SluggerInterface $slugger): Response
    {
        $em = $this->getDoctrine()->getManager();
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('imgFile')->getData();
            $em->persist($article);
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
            $article->setFilename($newFilename);
            $em->flush();
            return $this->redirectToRoute('article_index');
    }
        }
        return $this->render('article/create.html.twig', ['article' => $article, 'form' => $form->createView()]);
    }

    /**
     * @Route("/article/edit/{id}", name="article_edit")
     */
    public function editAction(int $id, Request $request,SluggerInterface $slugger)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID '.$id.' not found!');
        }

        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file = $editForm->get('imgFile')->getData();
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('image_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }
                $article->setFilename($newFilename);
                $em->flush();
                return $this->redirectToRoute('article_index');
            }

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'editForm' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/article/delete/{id}", name="article_delete")
     */
    public function deleteAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID'.$id.'not found!');
        }

        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('article_index');
    }
}
