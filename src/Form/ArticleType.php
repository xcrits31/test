<?php
namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ArticleType extends AbstractType
{
public function buildForm(FormBuilderInterface $builder, array $options)
{
$builder
    ->add('imgFile', FileType::class, [
        'label' => 'imgFile',
        'mapped' => false,
        'required' => true,
])
    ->add('name', TextType::class, [
        'label' => 'Название: ',
        'attr' => [
            'placeholder' => 'Введите название здесь...'
        ]
    ])
    ->add('description', TextType::class, [
        'label' => 'Описание: ',
        'attr' => [
            'placeholder' => 'Введите описание...',
        ]
    ])
    ->add('created_at', DateTimeType::class, [
        'label' => 'Дата создания: '
    ])
    ->add('submit', SubmitType::class, [
        'label' => 'Сохранить'
    ])
    ->add('reset', ResetType::class, [
        'label' => 'Сбросить'
    ]);
;
}

public function configureOptions(OptionsResolver $resolver)
{
$resolver->setDefaults([
'data_class' => Article::class,
]);
}
}